package ru.nsu.fit.g14201.moneyshark.database;


public class DatabaseException extends Throwable {
    public DatabaseException() {
    }

    public DatabaseException(String message) {
        super(message);
    }

    public DatabaseException(String message, Throwable cause) {
        super(message, cause);
    }

}
