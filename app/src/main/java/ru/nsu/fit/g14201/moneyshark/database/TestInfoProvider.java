package ru.nsu.fit.g14201.moneyshark.database;


import android.graphics.Color;

import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import ru.nsu.fit.g14201.moneyshark.model.Category;
import ru.nsu.fit.g14201.moneyshark.model.FinancialOperation;
import ru.nsu.fit.g14201.moneyshark.model.OperationType;

public class TestInfoProvider {
    private final static TestInfoProvider INSTANCE = new TestInfoProvider();

    public static TestInfoProvider getInstance() {
        return INSTANCE;
    }

    public List<Category> getCategoryTestInfo() {
        final List<Category> categories = new ArrayList<>();

        categories.add(new Category(OperationType.EXPENSES, "Food", Color.YELLOW));
        categories.add(new Category(OperationType.EXPENSES, "Transport", Color.CYAN));
        categories.add(new Category(OperationType.INCOME, "Programmer job", Color.GREEN));
        categories.add(new Category(OperationType.EXPENSES, "Child care", Color.RED));
        categories.add(new Category(OperationType.INCOME, "Bob's scholarship"));

        return categories;
    }

    public List<FinancialOperation> getFinancialOperationTestInfo(List<Category> categories) {
        final List<FinancialOperation> operations = new ArrayList<>();

        operations.add(new FinancialOperation(new BigDecimal(12005), categories.get(3),
                new DateTime(2017, 10, 15, 0, 0)));
        operations.add(new FinancialOperation(new BigDecimal(45), categories.get(1),
                new DateTime(2017, 11, 3, 0, 0)));
        operations.add(new FinancialOperation(new BigDecimal(40000), categories.get(2),
                new DateTime(2017, 11, 25, 0, 0)));
        operations.add(new FinancialOperation(new BigDecimal(198.75), categories.get(0),
                new DateTime(2017, 12, 5, 0, 0)));

        operations.add(new FinancialOperation(new BigDecimal(1348.52), categories.get(4),
                new DateTime(2017, 12, 9, 0, 0)));
        operations.add(new FinancialOperation(new BigDecimal(5001), categories.get(4),
                new DateTime(2017, 12, 10, 0, 0)));
        operations.add(new FinancialOperation(new BigDecimal(7201), categories.get(4),
                new DateTime(2017, 12, 16, 0, 0)));

        return operations;
    }
}
