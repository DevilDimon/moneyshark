package ru.nsu.fit.g14201.moneyshark.model;


import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import org.joda.time.DateTime;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import static ru.nsu.fit.g14201.moneyshark.database.DatabaseContract.FinancialOperationEntry;

@NoArgsConstructor
@DatabaseTable(tableName = FinancialOperationEntry.TABLE_NAME)
public class FinancialOperation {
    @Getter
    @DatabaseField(columnName = FinancialOperationEntry.ID, generatedId = true)
    private long id;

    @Getter
    @Setter
    @DatabaseField(columnName = FinancialOperationEntry.SUM, canBeNull = false)
    private BigDecimal sum;

    @Getter
    @Setter
    @DatabaseField(columnName = FinancialOperationEntry.CATEGORY,
            foreign = true, foreignAutoRefresh = true, maxForeignAutoRefreshLevel = 1)
    private Category category;

    @Getter
    @DatabaseField(columnName = FinancialOperationEntry.DATE)
    private DateTime date;

    public FinancialOperation(BigDecimal sum, Category category, DateTime date) {
        this.sum = sum;
        this.category = category;
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FinancialOperation that = (FinancialOperation) o;

        return id == that.id;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }
}
