package ru.nsu.fit.g14201.moneyshark.database;


import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.stmt.QueryBuilder;

import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.List;

import ru.nsu.fit.g14201.moneyshark.model.Category;
import ru.nsu.fit.g14201.moneyshark.model.FinancialOperation;

import static ru.nsu.fit.g14201.moneyshark.database.DatabaseContract.FinancialOperationEntry;

public class FinancialOperationDatabaseHelper {
    private RuntimeExceptionDao<FinancialOperation, Long> operationDao;

    public FinancialOperationDatabaseHelper(final DatabaseHelper databaseHelper) {
        operationDao = databaseHelper.getFinancialOperationRuntimeDao();
    }

    public List<FinancialOperation> getAllOperationsByCategoryAndPeriod(final Category category,
                                                                        final DateTime startPoint,
                                                                        final DateTime endPoint)
            throws SQLException {
        return operationDao.queryBuilder()
                .where()
                .eq(FinancialOperationEntry.CATEGORY, category)
                .and()
                .between(FinancialOperationEntry.DATE, startPoint, endPoint)
                .query();
    }

    public BigDecimal getOverallBalanceByCategoryAndPeriod(final Category category,
                                                           final DateTime startPoint,
                                                           final DateTime endPoint)
            throws SQLException {
        QueryBuilder<FinancialOperation, Long> queryBuilder = operationDao.queryBuilder();

        queryBuilder.selectRaw("SUM(" + FinancialOperationEntry.SUM + ")");
        queryBuilder.where()
                .eq(FinancialOperationEntry.CATEGORY, category)
                .and()
                .between(FinancialOperationEntry.DATE, startPoint, endPoint);

        final GenericRawResults<Object[]> results =
                operationDao.queryRaw(
                        queryBuilder.prepareStatementString(),
                        new DataType[]{DataType.BIG_DECIMAL});

        Object result = results.getFirstResult()[0];
        return (result == null) ? new BigDecimal(0) : (BigDecimal) result;
    }
}
