package ru.nsu.fit.g14201.moneyshark.database;


public final class DatabaseContract {
    private DatabaseContract() {
    }

    public static class FinancialOperationEntry {
        public static final String TABLE_NAME = "finance_operations";
        public static final String ID = "id";
        public static final String SUM = "sum";
        public static final String CATEGORY = "category";
        public static final String DATE = "date";
    }

    public static class CategoryEntry {
        public static final String TABLE_NAME = "categories";
        public static final String ID = "id";
        public static final String OPERATION_TYPE = "operation_type";
        public static final String NAME = "name";
        public static final String COLOR = "color";
    }
}
