package ru.nsu.fit.g14201.moneyshark.database;


import com.j256.ormlite.dao.RuntimeExceptionDao;

import java.sql.SQLException;
import java.util.List;

import ru.nsu.fit.g14201.moneyshark.model.Category;
import ru.nsu.fit.g14201.moneyshark.model.OperationType;


public class CategoryDatabaseHelper {
    private RuntimeExceptionDao<Category, Long> categoryDao;

    public CategoryDatabaseHelper(final DatabaseHelper databaseHelper) {
        categoryDao = databaseHelper.getCategoryRuntimeDao();
    }

    public Category getCategoryById(long id) {
        return categoryDao.queryForId(id);
    }

    public Category getCategoryByNameAndType(String name, OperationType type)
            throws SQLException, DatabaseException {
        List<Category> categories = categoryDao.queryBuilder()
                .where()
                .eq(DatabaseContract.CategoryEntry.NAME, name)
                .and()
                .eq(DatabaseContract.CategoryEntry.OPERATION_TYPE, type)
                .query();

        if (categories == null || categories.size() != 1) {
            throw new DatabaseException("found more than one result");
        } else {
            return categories.get(0);
        }
    }

    public List<Category> getAllCategories() {
        return categoryDao.queryForAll();
    }

    public void addCategory(final Category category) throws DatabaseException {
        if (categoryDao.queryForId(category.getId()) != null) {
            throw new DatabaseException("category already exists");
        }
        categoryDao.create(category);
    }

    public void updateCategory(final Category category) {
        categoryDao.update(category);
    }
}
