package ru.nsu.fit.g14201.moneyshark.database;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;
import java.util.List;

import ru.nsu.fit.g14201.moneyshark.model.Category;
import ru.nsu.fit.g14201.moneyshark.model.FinancialOperation;

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {
    private static final String DATABASE_NAME = "user_finances.db";
    private static final int DATABASE_VERSION = 1;

    private static DatabaseHelper instance;

    private RuntimeExceptionDao<Category, Long> categoryRuntimeDao;
    private RuntimeExceptionDao<FinancialOperation, Long> finOperationRuntimeDao;

    public DatabaseHelper(final Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static synchronized DatabaseHelper getHelper(Context context) {
        if (instance == null) {
            instance = new DatabaseHelper(context);
        }

        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, Category.class);
            TableUtils.createTable(connectionSource, FinancialOperation.class);
            fillDatabaseWithTestInfo();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onUpgrade(final SQLiteDatabase database, final ConnectionSource connectionSource, final int oldVersion, final int newVersion) {
    }

    @Override
    public void close() {
        finOperationRuntimeDao = null;
        categoryRuntimeDao = null;

        super.close();
        instance = null;
    }

    public RuntimeExceptionDao<Category, Long> getCategoryRuntimeDao() {
        if (categoryRuntimeDao == null) {
            categoryRuntimeDao = getRuntimeExceptionDao(Category.class);
        }
        return categoryRuntimeDao;
    }

    public RuntimeExceptionDao<FinancialOperation, Long> getFinancialOperationRuntimeDao() {
        if (finOperationRuntimeDao == null) {
            finOperationRuntimeDao = getRuntimeExceptionDao(FinancialOperation.class);
        }
        return finOperationRuntimeDao;
    }

    private void fillDatabaseWithTestInfo() {
        TestInfoProvider testInfoProvider = TestInfoProvider.getInstance();
        List<Category> categories = testInfoProvider.getCategoryTestInfo();

        getCategoryRuntimeDao().create(categories);
        getFinancialOperationRuntimeDao().create(
                testInfoProvider.getFinancialOperationTestInfo(categories));
    }
}
