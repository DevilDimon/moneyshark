package ru.nsu.fit.g14201.moneyshark.activity;

import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.util.Log;

import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.List;

import butterknife.BindView;
import ru.nsu.fit.g14201.moneyshark.MoneysharkApplication;
import ru.nsu.fit.g14201.moneyshark.R;
import ru.nsu.fit.g14201.moneyshark.database.CategoryDatabaseHelper;
import ru.nsu.fit.g14201.moneyshark.database.DatabaseHelper;
import ru.nsu.fit.g14201.moneyshark.database.FinancialOperationDatabaseHelper;
import ru.nsu.fit.g14201.moneyshark.fragment.AddDataFragment;
import ru.nsu.fit.g14201.moneyshark.fragment.CategoriesFragment;
import ru.nsu.fit.g14201.moneyshark.fragment.GraphFragment;
import ru.nsu.fit.g14201.moneyshark.model.Category;
import ru.nsu.fit.g14201.moneyshark.model.FinancialOperation;
import ru.nsu.fit.g14201.moneyshark.model.OperationType;

public class MainActivity extends BaseActivity
        implements
        AddDataFragment.OnFragmentInteractionListener,
        CategoriesFragment.OnFragmentInteractionListener {
    protected DatabaseHelper databaseHelper;

    @BindView(R.id.navigation)
    BottomNavigationView navigation;

    private BottomNavigationView.OnNavigationItemSelectedListener onNavigationItemSelectedListener
            = item -> {
        switch (item.getItemId()) {
            case R.id.navigation_add:
                // What to do upon tab change
                setFragment(AddDataFragment.newInstance(null, null));
                return true;
            case R.id.navigation_dashboard:
                setFragment(GraphFragment.newInstance(new FinancialOperationDatabaseHelper(databaseHelper),
                        new CategoryDatabaseHelper(databaseHelper)));
                return true;
            case R.id.navigation_categories:
                setFragment(CategoriesFragment.newInstance(null, null));
                return true;
        }
        return false;
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        navigation.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener);

        databaseHelper = DatabaseHelper.getHelper(this);
        if (savedInstanceState == null) {
            setFragment(GraphFragment.newInstance(new FinancialOperationDatabaseHelper(databaseHelper),
                    new CategoryDatabaseHelper(databaseHelper)));
            navigation.setSelectedItemId(R.id.navigation_dashboard);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        databaseHelper.close();
    }

    @Override
    int getLayoutId() {
        return R.layout.activity_main;
    }

    private void setFragment(Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .commit();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
