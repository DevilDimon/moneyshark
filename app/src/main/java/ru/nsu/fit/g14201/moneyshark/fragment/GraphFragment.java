package ru.nsu.fit.g14201.moneyshark.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.ToggleButton;
import android.widget.ViewSwitcher;

import com.borax12.materialdaterangepicker.date.DatePickerDialog;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.utils.ColorTemplate;

import org.joda.time.DateTime;
import org.joda.time.Years;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import butterknife.BindView;
import ru.nsu.fit.g14201.moneyshark.R;
import ru.nsu.fit.g14201.moneyshark.database.CategoryDatabaseHelper;
import ru.nsu.fit.g14201.moneyshark.database.FinancialOperationDatabaseHelper;
import ru.nsu.fit.g14201.moneyshark.model.Category;
import ru.nsu.fit.g14201.moneyshark.model.FinancialOperation;
import ru.nsu.fit.g14201.moneyshark.model.OperationType;

public class GraphFragment extends BaseFragment implements DatePickerDialog.OnDateSetListener {

    private static final int ANIMATION_DURATION_MS = 300;

    @BindView(R.id.view_switcher)
    ViewSwitcher viewSwitcher;

    @BindView(R.id.toggle_button)
    ToggleButton toggleButton;

    private PieChart pieChart;
    private BarChart barChart;

    private HashMap<Category, BigDecimal> sumsByCategory;
    private HashMap<Category, List<FinancialOperation>> operationsByCategory;

    private Period currentPeriod = Period.WEEK;
    private DateTime lastFromDate;
    private DateTime lastToDate;

    private FinancialOperationDatabaseHelper financialOperationDatabaseHelper;
    private CategoryDatabaseHelper categoryDatabaseHelper;

    public GraphFragment() {
    }

    public static GraphFragment newInstance(FinancialOperationDatabaseHelper financialOperationDatabaseHelper,
                                            CategoryDatabaseHelper categoryDatabaseHelper) {
        GraphFragment graphFragment = new GraphFragment();
        graphFragment.financialOperationDatabaseHelper = financialOperationDatabaseHelper;
        graphFragment.categoryDatabaseHelper = categoryDatabaseHelper;
        return graphFragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_graph;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        pieChart = new PieChart(getContext());
        pieChart.setTouchEnabled(false);
        pieChart.getLegend().setEnabled(false);
        pieChart.setDescription(null);
        pieChart.setDrawCenterText(false);
        pieChart.setHoleRadius(30.f);
        pieChart.setTransparentCircleRadius(35.f);
        pieChart.setDescription(null);


        barChart = new BarChart(getContext());
        barChart.setTouchEnabled(false);
        barChart.getLegend().setEnabled(false);
        barChart.setDescription(null);
        barChart.setDrawBorders(true);


        viewSwitcher.addView(pieChart);
        viewSwitcher.addView(barChart);
        viewSwitcher.setOnClickListener((v) -> viewSwitcher.showNext());

        toggleButton.setOnClickListener((v) -> reloadAll());

        reloadAll();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.graph_menu, menu);

        MenuItem item = menu.findItem(R.id.spinner);
        Spinner spinner = (Spinner) item.getActionView();
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.menu_time_periods_array, R.layout.spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0:
                        currentPeriod = Period.WEEK;
                        reloadAll();
                        break;
                    case 1:
                        currentPeriod = Period.MONTH;
                        reloadAll();
                        break;
                    case 2:
                        currentPeriod = Period.CUSTOM;
                        showDatePicker();
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // Do nothing
            }
        });

        spinner.setAdapter(adapter);
    }

    private void showDatePicker() {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dialog = DatePickerDialog.newInstance(this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH),
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH));

        dialog.show(getActivity().getFragmentManager(), DatePickerDialog.class.getSimpleName());
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth,
                          int yearEnd, int monthOfYearEnd, int dayOfMonthEnd) {
        DateTime dateStart = new DateTime(year, monthOfYear + 1, dayOfMonth, 0, 0);
        DateTime dateEnd = new DateTime(yearEnd, monthOfYearEnd + 1, dayOfMonthEnd, 0, 0);
        if (dateStart.isAfter(DateTime.now()) || dateEnd.isAfter(DateTime.now())) {
            showSnack(getString(R.string.toast_wrong_date_range));
            return;
        }
        if (Years.yearsBetween(dateStart, dateEnd).getYears() > 0) {
            showSnack(getString(R.string.toast_wrong_date_range));
            return;
        }

        lastFromDate = dateStart;
        lastToDate = dateEnd;

        reloadAll();
    }

    private void reloadAll() {
        reloadDataSet();
        reloadData();
    }

    private void reloadDataSet() {
        HashMap<Category, BigDecimal> newSums = new HashMap<>();
        HashMap<Category, List<FinancialOperation>> newOperations = new HashMap<>();
        DateTime start = null;
        DateTime end = null;
        switch (currentPeriod) {
            case WEEK:
                start = DateTime.now().withDayOfWeek(1);
                end = DateTime.now().withDayOfWeek(7);
                break;
            case MONTH:
                start = DateTime.now().withDayOfMonth(1);
                end = DateTime.now().withDayOfMonth(DateTime.now().dayOfMonth().getMaximumValue());
                break;
            case CUSTOM:
                start = lastFromDate;
                end = lastToDate;
                break;
        }

        List<Category> categories = categoryDatabaseHelper.getAllCategories();
        for (Category category : categories) {
            if (category.getOperationType() != getOperationType()) {
                continue;
            }

            try {
                BigDecimal sum = financialOperationDatabaseHelper.getOverallBalanceByCategoryAndPeriod(category, start, end);
                newSums.put(category, sum);

                List<FinancialOperation> operations = financialOperationDatabaseHelper.getAllOperationsByCategoryAndPeriod(category, start, end);
                newOperations.put(category, operations);
            } catch (SQLException e) {
                showSnack(getString(R.string.error_database));
                e.printStackTrace();
                return;
            }
        }

        sumsByCategory = newSums;
        operationsByCategory = newOperations;
    }

    private void reloadData() {
        reloadPieChart();
        reloadBarChart();
    }

    private void reloadBarChart() {

        ArrayList<BarEntry> barEntries = new ArrayList<>();
        switch (currentPeriod) {
            case WEEK:
                for (int i = 1; i <= 7; i++) {
                    Comparator<Category> comparator = (c1, c2) -> c1.getName().compareTo(c2.getName());
                    Map<Category, Float> heights = new TreeMap<>(comparator);
                    for (Map.Entry<Category, List<FinancialOperation>> entry : operationsByCategory.entrySet()) {
                        Category category = entry.getKey();
                        List<FinancialOperation> operations = entry.getValue();
                        for (FinancialOperation operation : operations) {
                            if (i == operation.getDate().getDayOfWeek()) {
                                if (heights.containsKey(category)) {
                                    heights.put(category, heights.get(category) + operation.getSum().floatValue());
                                } else {
                                    heights.put(category, operation.getSum().floatValue());
                                }
                            }
                        }
                    }

                    float[] primitiveHeights = new float[operationsByCategory.size()];
                    int j = 0;
                    for (Float f : heights.values()) {
                        primitiveHeights[j++] = f;
                    }

                    BarEntry barEntry = new BarEntry(i + 1, primitiveHeights);
                    barEntries.add(barEntry);
                }
                break;
            case MONTH:
                for (int i = 1; i <= DateTime.now().dayOfMonth().getMaximumValue(); i++) {
                    Comparator<Category> comparator = (c1, c2) -> c1.getName().compareTo(c2.getName());
                    Map<Category, Float> heights = new TreeMap<>(comparator);
                    for (Map.Entry<Category, List<FinancialOperation>> entry : operationsByCategory.entrySet()) {
                        Category category = entry.getKey();
                        List<FinancialOperation> operations = entry.getValue();
                        for (FinancialOperation operation : operations) {
                            if (i == operation.getDate().getDayOfMonth()) {
                                if (heights.containsKey(category)) {
                                    heights.put(category, heights.get(category) + operation.getSum().floatValue());
                                } else {
                                    heights.put(category, operation.getSum().floatValue());
                                }
                            }
                        }
                    }

                    float[] primitiveHeights = new float[operationsByCategory.size()];
                    int j = 0;
                    for (Float f : heights.values()) {
                        primitiveHeights[j++] = f;
                    }

                    BarEntry barEntry = new BarEntry(i + 1, primitiveHeights);
                    barEntries.add(barEntry);
                }
                break;
            case CUSTOM:
                for (DateTime day = lastFromDate; day.isBefore(lastToDate); day = day.plusDays(1)) {
                    Comparator<Category> comparator = (c1, c2) -> c1.getName().compareTo(c2.getName());
                    Map<Category, Float> heights = new TreeMap<>(comparator);
                    for (Map.Entry<Category, List<FinancialOperation>> entry : operationsByCategory.entrySet()) {
                        Category category = entry.getKey();
                        List<FinancialOperation> operations = entry.getValue();
                        for (FinancialOperation operation : operations) {
                            if (day.getDayOfYear() == operation.getDate().getDayOfYear()) {
                                if (heights.containsKey(category)) {
                                    heights.put(category, heights.get(category) + operation.getSum().floatValue());
                                } else {
                                    heights.put(category, operation.getSum().floatValue());
                                }
                            }
                        }
                    }

                    float[] primitiveHeights = new float[operationsByCategory.size()];
                    int j = 0;
                    for (Float f : heights.values()) {
                        primitiveHeights[j++] = f;
                    }

                    BarEntry barEntry = new BarEntry(day.getDayOfYear(), primitiveHeights);
                    barEntries.add(barEntry);
                }
                break;
        }
        BarDataSet barDataSet = new BarDataSet(barEntries, toggleButton.isChecked() ?
                getString(R.string.button_income) : getString(R.string.button_expenses));

        int[] colors = new int[operationsByCategory.size()];
        for (int i = 0; i < colors.length; i++) {
            colors[i] = ColorTemplate.MATERIAL_COLORS[i % ColorTemplate.MATERIAL_COLORS.length];
        }
        barDataSet.setColors(colors);

        BarData barData = new BarData(barDataSet);
        barChart.setData(barData);
        barChart.invalidate();
        barChart.animateXY(ANIMATION_DURATION_MS, ANIMATION_DURATION_MS);
    }

    private void reloadPieChart() {

        ArrayList<PieEntry> pieEntries = new ArrayList<>();
        ArrayList<Integer> pieColors = new ArrayList<>();
        for (Map.Entry<Category, BigDecimal> entry : sumsByCategory.entrySet()) {
            pieEntries.add(new PieEntry(entry.getValue().floatValue(), entry.getKey().getName()));
            int color = entry.getKey().getColor();
            pieColors.add(color);
        }

        PieDataSet pieDataSet = new PieDataSet(pieEntries, toggleButton.isChecked() ?
                getString(R.string.button_income) : getString(R.string.button_expenses));
        pieDataSet.setColors(pieColors);
        PieData pieData = new PieData(pieDataSet);
        pieChart.setData(pieData);
        pieChart.invalidate();
        pieChart.animateXY(ANIMATION_DURATION_MS, ANIMATION_DURATION_MS);
    }

    private OperationType getOperationType() {
        return toggleButton.isChecked() ? OperationType.INCOME : OperationType.EXPENSES;
    }

    private enum Period {
        WEEK, MONTH, CUSTOM
    }
}
