package ru.nsu.fit.g14201.moneyshark;


import android.app.Application;

import com.facebook.stetho.Stetho;

import net.danlew.android.joda.JodaTimeAndroid;

public class MoneysharkApplication extends Application {
    private final static String TAG = "safari";

    @Override
    public void onCreate() {
        super.onCreate();
        Stetho.initializeWithDefaults(this);
        JodaTimeAndroid.init(this);
    }

    public static String getTag() {
        return TAG;
    }
}
