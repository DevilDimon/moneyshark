package ru.nsu.fit.g14201.moneyshark.model;

import android.graphics.Color;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import static ru.nsu.fit.g14201.moneyshark.database.DatabaseContract.CategoryEntry;

@NoArgsConstructor
@DatabaseTable(tableName = CategoryEntry.TABLE_NAME)
public class Category {
    @Getter
    @DatabaseField(columnName = CategoryEntry.ID, generatedId = true)
    private long id;

    @Getter
    @DatabaseField(columnName = CategoryEntry.OPERATION_TYPE, canBeNull = false)
    private OperationType operationType;

    @Getter
    @DatabaseField(columnName = CategoryEntry.NAME, canBeNull = false)
    private String name;

    @DatabaseField(columnName = CategoryEntry.COLOR, canBeNull = false)
    @Getter
    @Setter
    private int color;

    public Category(OperationType operationType, String name) {
        this(operationType, name, Color.BLUE);
    }

    public Category(OperationType operationType, String name, int color) {
        this.operationType = operationType;
        this.name = name;
        this.color = color;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Category category = (Category) o;

        return id == category.id;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }
}
