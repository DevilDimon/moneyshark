package ru.nsu.fit.g14201.moneyshark.model;


public enum OperationType {
    INCOME, EXPENSES
}
